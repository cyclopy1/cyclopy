# CycloPY

The CycloPY website

## Commands

```bash
# Generate site
## simple generation
pelican content
## with a specific theme
pelican content -t /projects/your-site/themes/your-theme


# Serve content
pelican --listen
```