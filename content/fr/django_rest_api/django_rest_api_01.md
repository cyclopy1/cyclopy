---
Title: Django REST API 01 - Création du projet
Date: 2021-03-29 01:09
Category: Tutorials
Tags: Python, Django
Slug: Django-REST-API-01
Authors: Marc-Aurele Coste
Lang: Fr
Thumbnail: https://i.ibb.co/c60CTX4/django-rest-api-01.png
---

Dans cette introduction on va voir comment créer un projet Django.

Pour les personnes qui ont déjà créé un projet Django elles peuvent passer directement à la partie deux dans laquelle on entre réellement dans le vif du sujet, à savoir créer une API REST avec Django.

Les sources pour cette partie sont disponible [ici](https://gitlab.com/cyclopy1/django-rest-api/-/tree/%231). (Il faudra appliquer les migrations)

---

## Installation

On commence par créer un dossier et un virtualenv pour notre nouveau projet.

```bash
# Création du dossier pour notre projet
# #####################################
mkdir django_api && cd django_api

# Création du virtualenv
# ######################

# Ici avec le paquet virtualenv
python3 -m venv .venv
source .venv/bin/activate  # On active le venv
# Ici avec conda
conda create --prefix .venv python=3.8
conda activate ./.venv  # On active le venv

pip install -U pip setuptools wheel  # On met à jour quelques paquets

# On crée un fichier de "requirements" qui nous servira à installer nos dépendances
# #################################################################################
touch requirements.txt && echo Django >> requirements.txt
pip install -r requirements.txt
```

On vérifie que l'installation de Django a bien été faite grâce à la commande `django-admin`.
```bash
django-admin help
```

Normalement, si l'installation s'est bien passée, nous devrions voir quelque chose de similaire à ceci.

![django_admin_help](https://i.ibb.co/3SGbmBF/django-admin-help.png)

---

## Création du projet

On lance la création du projet Django avec la commande `django-admin` vue un peu plus haut.

```bash
django-admin startproject DjangoTodo && cd DjangoTodo
```

On se trouve dans le dossier DjangoTodo et on peut voit un fichier **manage.py** ainsi qu'un autre dossier nommé **DjangoTodo** dans lequel nous allons notament retrouver le fichier **settings.py**.

> Il faut noter qu'à présent que le projet est créé, il faudra passer par le `manage.py` pour lancer les commandes fournies par Django et non pas par `django-admin`

On lance tout de suite un petit `check` suivi d'un `runserver` pour être sûr que tout est ok.
```bash
# Petit check pour être sur qu'il n'y a pas d'erreur
python manage.py check
# On lance le project
python manage.py runserver
```

Le serveur de développement se lance mais on peut également voir un message en rouge nous informant que nous n'avons pas appliqué 18 migrations.

![unapplied_migrations](https://i.ibb.co/xFQsbq2/unapplied-migrations.png)

Django est en effet fourni avec beaucoup de composants dont la gestion des permissions, des utilisateurs et des groupes. Pour régler ce souci, on lance la commande

```bash
python manage.py migrate
```

Une fois les migrations effectuées on peut de nouveau lancer la commande `runserver` et en se rendant sur [127.0.0.1:8000](http://127.0.0.1:8000) on doit voir

![django_success](https://i.ibb.co/6vf8D0c/django-success.png)

---

## Conclusion

Voilà on a maintenant un projet Django fonctionnel. Prochaine étape mettre en place l'API REST avec le paquet **djangorestframework**.