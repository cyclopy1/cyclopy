---
Title: Django REST API 02 - Django REST framework
Date: 2021-03-29 01:10
Category: Tutorials
Tags: Python, Django
Slug: Django-REST-API-02
Authors: Marc-Aurele Coste
Lang: Fr
Thumbnail: https://i.ibb.co/sw9DdK2/django-rest-api-02.png
---

On arrive ici à la deuxieme étape. Dans cette partie on va installer `djangorestframework` et on va le configurer pour fonctionner avec notre projet. Lorsque ce sera fait on passera à la création de notre première resources.

Les sources pour cette partie sont disponible [ici](https://gitlab.com/cyclopy1/django-rest-api/-/tree/%232).

---

## Installation de djangorestframework

On ajoute **djangorestframework** à notre fichier **requirements.txt** et on lance l'installation avec pip.

```bash
echo djangorestframework >> requirements.txt
pip install -r requirements.txt
```

Une fois l'installation terminée on ajoute *rest_framework* à notre liste des *INSTALLED_APPS* dans le fichier **settings.py**.

```python
# settings.py
...
INSTALLED_APPS=[
    ...
    # Django rest framework
    'rest_framework'
]
...
```

---

## Création de l'API

### 1. Creation de l'application Django

On va commencer par créer une *"Django application"* en utilisant le script **manage.py**.

```bash
python manage.py startapp todo
```

On se retrouve avec un nouveau dossier *todo* avec les fichiers suivants:
- **admin.py**
- **apps.py**
- **models.py**
- **tests.py**
- **views.py**

Pour finir on ajoute notre nouvelle application à la liste des **INSTALLED_APPS** afin qu'elle soit détectée et chargée par Django lorsque l'on lancera le projet avec `runserver`.

```python
# ...
INSTALLED_APPS = [
    #...
    # Our Todo app
    'todo'
]
# ...
```

### 2. Création du model Todo

Passons maintenant à la création de notre première ressource (en django on parle de *model*). On va réaliser une API REST qui nous permettra de gérer une *todo* liste et notre première ressource sera donc notre modèle de **todo** qui aura les attributs suivants :

- **title** : Le titre de notre todo
- **description** : Une description si nécessaire de notre todo, cette description ne sera pas obligatoire
- **finished** : L'état de notre todo, cela nous permettra notamment de filtrer les todos déjà finis

```python
class Todo(models.Model):
    title = models.CharField(max_length=150, null=False, blank=False)
    description = models.TextField()
    finished = models.BooleanField(default=False)
```

On a maintenant notre classe qui représente notre première ressource. Il faut maintenant créer une migration pour permettre l'utilisation et le stockage en base de données de cette ressource. Pour ce faire on va suivre les trois étapes suivantes :

1. On utilise Django pour créer notre migration (avec la commande `makemigrations`)
2. On vérifie notre migration
3. On applique la migration sur notre base de données (avec la commande `migrate`)

```bash
# Creation de la migration
python manage.py makemigrations
```

Un nouveau fichier **0001_initial.py** est créé dans notre dossier de **migrations** avec le contenu suivant:

```python
from django.db import migrations, models


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Todo',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('title', models.CharField(max_length=150)),
                ('description', models.TextField(blank=True, null=True)),
                ('finished', models.BooleanField(default=False)),
            ],
        ),
    ]
```
On voit ici que nous créons un nouveau modèle **Todo** avec `migration.CreateModel` et il dispose de quatre champs:
- **id**
- **title**
- **description**
- **finished**

Django ajoute automatiquement la *primary key* **id** à notre ressource c'est pour cela qu'on n'a pas besoin de l'ajouter dans le code de notre classe. Le fichier de migration est conforme à ce que l'on attend on peut maintenant appliquer la migration.

```bash
# Nous appliquons la migration sur notre base de donnee
python manage.py migrate
```

### 3. L'admin Django

Un des gros atouts de Django c'est son interface d'admin. Cette interface permet d'administrer nos modèles facilement. Cette interface d'admin est protégé par un mot de passe il faut donc créer un compte super utilisateur pour y accéder.

```bash
python manage.py createsuperuser
```

On se rend ensuite sur la page d'[admin](http://127.0.0.1:8000/admin), on s'authentifie et normalement on devrait voir la page suivante:

![admin_first_login](https://i.ibb.co/J7dCKK6/admin-first-login.png)

On voit que l'on peut administrer des utilisateurs et des groupes. En revanche, on ne voit pas notre ressource. Pour voir une ressource dans l'interface d'admin il faut l'enregistrer. On va donc aller dans le fichier **admin.py** dans notre dossier *todo* et on ajoute le code suivant :

```python
from django.contrib import admin

from .models import Todo


# Register your models here.
admin.site.register(Todo)
```

> Ici on enregistre de façon basique notre ressource, mais Django propose de nombreuses possibilités de customisation pour définir ce qui doit être affiché et comment. Pour plus d'informations vous pouvez vous rendre [ici](https://docs.djangoproject.com/en/3.1/ref/contrib/admin/)

### 4. Serializer

Dans **djangorestframework** on utilise des *serializers* pour transformer nos ressources en JSON et inversement. C'est dans un *serializer* que l'on va définir quels champs d'un *todo* seront retournés et quels champs sont attendus lorsque l'on souhaite en créer un.

Pour des questions d'organisation on va créer un nouveau module python (un fichier python) où on définira notre *serializer*. On crée notre module **serializers.py** dans lequel on met le code suivant:

```python
from rest_framework.serializers import ModelSerializer

from .models import Todo


class TodoSerializer(ModelSerializer):
    class Meta:
        model = Todo
        fields = ('id', 'title', 'description', 'finished')
```

### 5. Viewset

On a notre *serializer*, il faut maintenant créer la vue dans laquelle on va l'utiliser. **Djangorestframework** met à notre disposition la classe **ModelViewSet** qui nous permet de créer facilement nos vues CRUD.
- `C`reate
- `R`ead
- `U`pdate
- `D`elete

Cette classe a deux attributs:
- **queryset**: La requête SQL qui sera utilisée comme base pour notre vue.
- **serializer_class**: La class qui sera utilisée pour la serialization et la déserialization des resultats de la query.

```python
from rest_framework import viewsets

from .models import Todo
from .serializers import TodoSerializer


# Create your views here.
class TodoViewset(viewsets.ModelViewSet):

    queryset = Todo.objects.all()
    serializer_class = TodoSerializer
```


On obtient les cinq vues suivante:

| HTTP méthode | CRUD | Description                            | Endpoint             |
|--------------|------|----------------------------------------|----------------------|
| GET          |   R  | Récupération de la liste des todos     | /api/v1/todos/        |
| POST         |   C  | Création d'un nouveau todo             | /api/v1/todos/        |
| GET          |   R  | Récupération du todo avec l'id **id**  | /api/v1/todos/**id**  |
| PUT          |   U  | Mise à jour du todo avec l'id **id**   | /api/v1/todos/**id**  |
| DELETE       |   D  | Suppression du todo avec l'id **id**   | /api/v1/todos/**id**  |


> Pour les *endpoints* il faut finir la partie 6 sur les URLs.

### 6. URLs

Pour que notre application soit fonctionnelle il faut maintenant lier nos vues aux URLs que l'on veut utiliser. Encore une fois **djangorestframework** propose un système prêt à l'emploi, les *router*. Grâce à ces *router* on peut enregistrer notre *viewset* directement et c'est le routeur qui se chargera de faire l'association entre la méthode HTTP utilisé et la fonction de notre viewset à appeler.

On crée le fichier **urls.py** de notre application *todo* et on y ajoute notre router.

```python
from rest_framework.routers import DefaultRouter
from .views import TodoViewset


TODO_ROUTER = DefaultRouter()
TODO_ROUTER.register(r'^', TodoViewset)
```

Pour finir il faut référencer ces nouvelles URLs dans le fichier **urls.py** que l'on retrouve dans le dossier portant le nom de notre projet (ici **DjangoTodo**).

```python
from django.contrib import admin
from django.urls import path, include

from todo.urls import TODO_ROUTER


API_URL = [
    path(r'todos', include(TODO_ROUTER.urls))
]

urlpatterns = [
    path('admin/', admin.site.urls),

    path(r'api/v1/', include(API_URL)),
]
```

On peut maintenant se rendre sur [http://127.0.0.1:8000/api/v1/todos/](http://127.0.0.1:8000/api/v1/todos/) et voir l'écran suivant:

![rest_framework_html](https://i.ibb.co/pbQbr6N/rest-framework-html.png)

---

## Conclusion

On a maintenant un projet fonctionnel avec une API REST accessible et qui nous permet d'interagir avec nos *todo*, on peut notamment les lister, les créer, les modifier et les supprimer.