---
Title: Django REST API 03 - Les filtres
Date: 2021-03-29 01:11
Category: Tutorials
Tags: Python, Django
Slug: Django-REST-API-03
Authors: Marc-Aurele Coste
Lang: Fr
Thumbnail: https://i.ibb.co/2y0M2RS/django-rest-api-03.png
---

On a une API qui permet de faire les opérations basiques avec nos *todos* mais en l'état lorsque l'on veut voir la liste de nos tâches on se retrouve avec **toutes** les tâches qu'elles soient fini ou non, bref ce n'est pas vraiment exploitable surtout lorsque le nombre de tâches augmentera. On va donc voir comment filtrer les résultats que doit nous retourner notre API.

Pour pouvoir faire ça nous allons installer un nouveau paquet **django-filter**. Ce paquet a l'avantage de s'interfacer parfaitement avec django-rest-framework.

---

## Installation de django-filter

On installe **django-filter** après l'avoir ajouté au fichier **requirements.txt**.

```bash
echo django-filter >> requirements.txt
pip install -r requirements.txt
```

Comme pour les autres applications qu'on a installées on va ajouter *django_filters* à notre liste des *INSTALLED_APPS* dans le fichier **settings.py**. Cette fois-ci il faudra en plus ajouter une configuration pour dire à *djangorestframework* quel *backend* il doit utiliser pour filtrer les résultats.

```python
INSTALLED_APPS = [
    ...
    'django_filters',
]

REST_FRAMEWORK = {
    ...
    'DEFAULT_FILTER_BACKENDS': ('django_filters.rest_framework.DjangoFilterBackend',)
}
```

---

## Filtrer directement dans les vues

Il y a plusieurs façons de filtrer nos résultats. Dans un premier temps on va voir la manière la plus simple à mettre en place. Pour ce faire on va agir directement au niveau de notre *viewset*.

Les sources avec les filtres au niveau des vues se trouvent [ici](https://gitlab.com/cyclopy1/django-rest-api/-/tree/%232.5).

### Filtrage basique

On va dans notre classe **TodoViewset** et ajouter l'attribut *filter_fields*.

```python
# views.py
...
class TodoViewset(viewsets.ModelViewSet):
    ...
    filter_fields = ('finished',)
```

On se rend sur [locahost:8000/api/todos/](http://locahost:8000/api/v1/todos/) et on peut voir en haut à droite un nouveau bouton **Filters**.

![filter_button](https://i.ibb.co/QHthRnt/filter-button.png)

On peut maintenant filtrer nos résultats et demander la liste des tâches qui ne sont pas encore finies (avec le champ *finished* à *false*).

### Filtrage avancé

Bon ok on peut filtrer nos tâches en utilisant le status *finished* c'est déjà bien, mais on peut faire mieux. Imaginons qu'on ait des centaines de *todos*, on peut alors se retrouver avec une liste énorme de *todos* qui ne sont pas finis. On va donc donner la possibilité aux utilisateurs de filtrer leurs tâches en se basant sur le titre et la description.

Pour cela on change l'attribut *filter_fields* en dictionnaire avec en clés les champs sur lesquels on veut filtrer et en valeurs la liste des *lookups* à appliquer.

```python
# views.py
...
class TodoViewset(viewsets.ModelViewSet):
    ...
    filter_fields = {
        'title': ('icontains',),
        'description': ('icontains',),
        'finished': ('exact',)
    }
```

> Ici on dit que l'on veut faire un test d'inclusion insensible à la casse (*icontains*) pour le titre et pour la description en revanche pour le status afin d'avoir le même comportement on utilise ici *exact*.

Maintenant lorsqu'on clique sur le bouton *filters* on devrait voir une pop-up suivante :

![filter_pop_up](https://i.ibb.co/tZHnxKC/filter-multiple-ids.png)

Voici quelques possibilités de lookups (une liste plus complète se trouve [ici](https://docs.djangoproject.com/fr/3.1/ref/models/querysets/#field-lookups))

| Type         | Description                                 |
|--------------|---------------------------------------------|
| `exact`      | Correspondance exacte.                      |
| `iexact`     | Correspondance exacte insensible à la casse |
| `contains`   | Test d’inclusion                            |
| `icontains`  | Test d’inclusion insensible à la casse      |
| `gt`         | Plus grand que                              |
| `lt`         | Plus petit que                              |
| `startswith` | Commence par (sensible à la casse)          |
| `endswith`   | Se termine par (sensible à la casse)        |

---

## Creation d'une classe pour filtrer

Nous savons maintenant filtrer nos tâches en paramétrant tout ça dans les vues. On va maintenant voir comment déplacer cette logique dans une classe dédiée. Cela permettra une meilleure lisibilité et une plus grande flexibilité. Grâce à ce système on pourra notamment ajouter des filtres sur des champs qui ne sont pas forcément présents dans le modèle.

Les sources avec une classe dédiée pour les filtres sont [ici](https://gitlab.com/cyclopy1/django-rest-api/-/tree/%233).

### Filtrage basique / avancé

Comme pour les vues, on va voir qu'il est possible de filtrer de manière simple et dans un second temps nous verrons comment obtenir le meme comportement que le filtrage "avancé" dans les vues et enfin on verra comment on peut aller plus loin.

On commence par creer un nouveau module **filters.py** dans lequel on va mettre notre classe pour le filtrage.

```python
# filters.py
import django_filters

from .models import Todo


class TodoFilter(django_filters.FilterSet):
    class Meta():
        model = Todo
        fields = ('finished',)
```

Maintenant on va remplacer dans notre classe **TodoViewset** l'attribut *filter_fields* par l'attribut *filter_class*.

```python
#views.py
...
from .filters import TodoFilter

class TodoViewset(viewsets.ModelViewSet):
    ...
    filter_class = TodoFilter
```

Nous avons ici le même comportement que pour le filtrage basique. On modifie maintenant cette classe de filtre pour obtenir le même comportement que pour le filtrage avancé.

```python
# filters.py
import django_filters

from .models import Todo


class TodoFilter(django_filters.FilterSet):
    class Meta():
        model = Todo
        fields = {
            'title': ('icontains',),
            'description': ('icontains',),
            'finished': ('exact',)
        }
```

### Filtrage aller plus loin

Nous avons retrouvé le même comportement que ce que nous avions lorsque l'on faisait tout dans les vues. Maintenant on va ajouter la possibilité de filtrer sur une liste d'*ids*. Pour cela on va ajouter un attribut *ids* dans notre classe **TodoFilter** qui sera du type *BaseInFilter* (fourni par le paquet *django-filter*).

```python
# filters.py
...
class TodoFilter(django_filters.FilterSet):
    ids = django_filters.BaseInFilter(field_name='id')

    class Meta():
        model = Todo
        fields = {
            'title': ('icontains', 'startswith'),
            'description': ('icontains',),
            'finished': ('exact',)
        }
```

Nous pouvons maintenant passer plusieurs ids séparés par des virgules comme on peut le voir sur l'image ci-dessous.

![filter_multiple_ids](https://i.ibb.co/frZdwHQ/filter-pop-up.png)

---

## Conclusion

Voilà, c'est tout pour la partie filtrage. Cela vous permettra d'obtenir une API assez flexible sur les données retournées et vous évitera un traitement côté client.