---
Title: Django REST API 04 - La pagination
Date: 2021-03-29 01:12
Category: Tutorials
Tags: Python, Django
Slug: Django-REST-API-04
Authors: Marc-Aurele Coste
Lang: Fr
Thumbnail: https://i.ibb.co/Wpr0119/django-rest-api-04.png
---

La pagination n'est pas forcément quelque chose que l'on met place dès le début d'un projet, il y a d'autres choses à gérer qui sont plus importantes. C'est une erreur, surtout avec *djangorestframework* qui inclut la gestion de la pagination. Il est important de mettre en place la pagination le plus rapidement possible, car comme on va le voir cela a un impact sur le retour de certains services.

On va mettre en place la pagination de deux manières distinctes. Dans un premier temps on va mettre en place une pagination globale. Avec ça on sera sûr que la pagination sera toujours présente et qu'on ne dépassera pas une certaine limite d'objets retournés avec nos services. Il faut voir ça comme une sécurité. Dans un second temps on verra comment gérer la pagination au niveau des vues.

---

## Mise en place global

On ajoute **DEFAULT_PAGINATION_CLASS** et **PAGE_SIZE** a notre configuration de *djangorestframework*

```python
# settings.py
REST_FRAMEWORK = {
    ...
    'DEFAULT_PAGINATION_CLASS': 'rest_framework.pagination.PageNumberPagination',
    'PAGE_SIZE': 100
}
```

Maintenant on se rend sur notre [liste des tâches](http://127.0.0.1:8000/api/v1/todos/) et on peut constater nos *todos* sont bien retournés comme prévu, en revanche le résultat n'est pas tout à fait le même. On est passé d'une simple liste d'objets à un objet plus complexe.

![results_before_after]()https://i.ibb.co/4WCnNZ8/before-after.png)

Cet objet dispose de quatre champs:
- **count**: C'est le nombre d'éléments retourne
- **next**: L'URL pour aller sur la page suivante s'il y en a une
- **previous**: L'URL pour aller sur la page précédente s'il y en a une
- **results**: La liste des résultats pour cette page

---

## Mise en place par vue

On a une pagination globale qui s'applique de la même façon à tous nos *endpoints*, mais parfois on veut pouvoir gérer la pagination de manière différente pour certains *endpoints*. Pour cela on va créer une classe dédiée. On commence par créer un nouveau module **pagination.py** et on y ajoute notre classe.

```python
# pagination.py
from rest_framework.pagination import PageNumberPagination


class TinyResultsSetPagination(PageNumberPagination):
    page_size = 2
    page_size_query_param = 'psize'
    max_page_size = 5
```

Maintenant on ajoute cette classe de pagination à notre **TodoViewset**.

```python
# views.py
...
from .pagination import TinyResultsSetPagination


class TodoViewset(viewsets.ModelViewSet):
    ...
    pagination_class = TinyResultsSetPagination
```

Et voilà maintenant lorsque l'on demande la liste de nos tâches nous n'avons plus que deux éléments à la fois qui sont retournés. Nous avons même la possibilité d'ajuster ce nombre grâce au paramètre d'URL `psize` que nous avons configuré dans notre classe de pagination.

Ainsi l'URL [http://127.0.0.1:8000/api/v1/todos/?psize=3](http://127.0.0.1:8000/api/v1/todos/?psize=3) retourne trois tâches.

---

## Conclusion

La pagination est plutôt simple à mettre en place avec *djangorestframework* et a un impact assez important sur les retours de notre API alors autant la mettre en place dès le début du projet.
