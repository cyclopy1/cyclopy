---
Title: Django REST API 05 - Les utilisateurs
Date: 2021-03-29 01:13
Category: Tutorials
Tags: Python, Django
Slug: Django-REST-API-05
Authors: Marc-Aurele Coste
Lang: Fr
Thumbnail: https://i.ibb.co/bJrfqDZ/django-rest-api-05.png
---

On a une API qui nous permet de faire pas mal de choses avec nos tâches, mais elle est publique et toute personne se rendant dessus peut obtenir la liste des tâches qu'on a créées. On va donc mettre en place deux choses :
1. Seul des utilisateurs authentifier pourront accéder à la liste des tâches
2. Les utilisateurs auront accès qu'aux tâches qu'ils auront créées.

Les sources pour cette partie sont disponible [ici](https://gitlab.com/cyclopy1/django-rest-api/-/tree/%235).

---

## Etape 1 : Authentification

On veut que seul les utilisateurs authentifiés aient accès aux tâches. On va donc définir dans les paramètres de *djangorestframework* quelle permission par défaut on veut avoir sur nos services et quels sont les types d'authentification que nous allons autoriser. Pour nous on utilisera comme type d'authentification :
- L'authentification basique
- L'authentification avec un token

> Pour pouvoir utiliser l'authentification avec un token il faut ajouter une nouvelle application, *rest_framework.authtoken*, dans nos **INSTALLED_APPS**. Il faudra également appliquer les migrations qui vont avec cette nouvelle application avec la commande `python manage.py migrate`.

```python
# settings.py
...
INSTALLED_APPS = [
    ...
    'rest_framework.authtoken',
    ...
]
...
REST_FRAMEWORK = {
    'DEFAULT_PERMISSION_CLASSES': (
        'rest_framework.permissions.IsAuthenticated',
    ),
    'DEFAULT_AUTHENTICATION_CLASSES': (
        'rest_framework.authentication.BasicAuthentication',
        'rest_framework.authentication.TokenAuthentication',
    ),
    ...
}
```

Maintenant lorsque l'on se rend sur notre application et qu'on n'est pas authentifié on peut voir une popup nous demandant un nom d'utilisateur et un mot de passe.

![login_popup](https://i.ibb.co/L6PqhS4/login-popup.png)

---

## Etape 2 : Gestion des utilisateurs

La première partie du problème est résolu maintenant il faut faire en sorte que les utilisateurs puissent voir la liste des tâches qu'ils ont créés et pas les tâches de tout le monde. On va donc lier les *todos* aux utilisateurs qui les ont créés.

### Modification du modele Todo

On va donc modifier notre modèle *Todo* pour lui rajouter un champ **owner** qui sera une clé étrangère (One-to-Many) vers le modèle User.

```python
# models.py
...
from django.contrib.auth.models import User


class Todo(models.Model):
    owner = models.ForeignKey(User, on_delete=models.CASCADE, null=False)
    ...
```

Il faut maintenant générer une nouvelle migration.

```bash
# On genere la migration.
python manage.py makemigrations

# On check la migration genere pour être sur que ce qui a ete genere
# correspond bien à ce que l'on souhaite.

# On applique la migration
python manage.py migrate
```

> Etant donnée que notre champ utilise `null=False` et que nous avons déjà des *todos* dans notre data base il va falloir mettre une valeur par défaut pour combler ces trous. On choisit **1** qui représente l'id de notre super-utilisateur.

### Lier les utilisateurs et les Todos

L'étape suivante c'est de faire en sorte que lorsqu'un de nos utilisateurs créent un *todo* il en soit automatiquement le propriétaire. Pour cela nous allons modifier notre TodoSerializer et plus précisément la fonction *create* dont nous héritons de notre parent.

```python
# serializers.py
...
class TodoSerializer(ModelSerializer):
    ...
    def create(self, validated_data):
        owner = self.context['request'].user
        validated_data['owner'] = owner
        return super().create(validated_data)
```

### Filter les résultats

Dernière étape, filtrer les résultats que l'on retourne en fonction de l'utilisateur qui fait la requête.

```python
# views.py
...
class TodoViewset(viewsets.ModelViewSet):
    ...
    def get_queryset(self):
        queryset = super().get_queryset()
        return queryset.filter(owner=self.request.user)
```

## Authtoken

Dernier petit point avant de conclure. On a autorisé l'utilisation d'un *authtoken* pour pouvoir faire des requêtes sur notre API. On va rapidement mettre en place de quoi exploiter ce système.

Dans notre fichier d'url global nous allons ajouter une nouvelle route qui permettra de générer ou récupérer le token pour un utilisateur.

```python
# urls.py
...
from rest_framework.authtoken import views

API_URL = [
    path(r'get-token', views.obtain_auth_token),
    ...
]
...
```

Voilà, il nous suffit de faire un post sur [127.0.0.1:8000/api/v1/get-token](http://127.0.0.1:8000/api/v1/get-token) avec un client REST (postman par example) avec les informations suivantes dans le body.

```jsonc
// Body de la requête
{
    "username": "<Nom d'utilisateur>",
    "password": "<Mot de passe>"
}

// Retour du service
{
    "token": "<Token>"
}
```

On peut l'utiliser en ajoutant dans le champs **Authorization** du header de nos requêtes `Token <Token>`.

Pour les utilisateurs de Postman, on peut voir que l'on a l'option *Bearer Token* mais le header génère ressemble à `Bearer <Token>` et pas `Token <Token>`. 

![postman_authorization_type](https://i.ibb.co/R9jsZm0/postman-authorization-type.png)

On va remédier à ça dessuite en ajoutant dans le dossier **DjangoTodo** un nouveau module **authentication.py** dans lequel on va mettre.

```python
from rest_framework.authentication import TokenAuthentication


class BearerTokenAuthentication(TokenAuthentication):
    keyword = 'Bearer'
```

Dans nos paramètres il suffit de remplacer dans la partie **REST_FRAMEWORK** `rest_framework.authentication.TokenAuthentication` avec `DjangoTodo.authentication.BearerTokenAuthentication`.

---

## Conclusion

Notre API est maintenant utilisable par de multiples utilisateurs sans que ceux-ci aient accès aux tâches de tout le monde.
